locals {
  bridge = <<-EOT
    %{for element in var.bridge}Bridge ${element}
    %{endfor}
  EOT

  exit_policy = <<-EOT
    %{for policy in var.exit_policy}ExitPolicy ${policy}
    %{endfor}
  EOT

  options = [
    chomp(local.bridge) == "" ? "" : join("", [chomp(local.bridge), "\n"]),
    var.bridge_distribution == null ? "" : "BridgeDistribution ${var.bridge_distribution}\n",
    var.bridge_relay == null ? "" : format("BridgeRelay %s\n", var.bridge_relay ? "1" : "0"),
    var.client_transport_plugin == null ? "" : "ClientTransportPlugin ${var.client_transport_plugin}\n",
    var.contact_info == null ? "" : "ContactInfo ${var.contact_info}\n",
    var.control_port == null ? "" : "ControlPort ${var.control_port}\n",
    var.dir_port == null ? "" : "DirPort ${var.dir_port}\n",
    var.dir_port_front_page == null ? "" : "DirPortFrontPage ${var.dir_port_front_page}\n",
    chomp(local.exit_policy) == "" ? "" : join("", [chomp(local.exit_policy), "\n"]),
    var.exit_relay == null ? "" : format("ExitRelay %s\n", var.exit_relay ? "1" : "0"),
    var.ext_or_port == null ? "" : "ExtORPort ${var.ext_or_port}\n",
    var.nickname == null ? "" : "Nickname ${var.nickname}\n",
    var.or_port == null ? "" : "ORPort ${var.or_port}\n",
    var.relay_bandwidth_burst == null ? "" : "RelayBandwidthBurst ${var.relay_bandwidth_burst}\n",
    var.relay_bandwidth_rate == null ? "" : "RelayBandwidthRate ${var.relay_bandwidth_rate}\n",
    var.server_transport_listen_addr == null ? "" : "ServerTransportListenAddr ${var.server_transport_listen_addr}\n",
    var.server_transport_plugin == null ? "" : "ServerTransportPlugin ${var.server_transport_plugin}\n",
    var.use_bridges == null ? "" : format("UseBridges %s\n", var.use_bridges ? "1" : "0")
  ]

  joined_options = join("", local.options)
}
