variable "bridge" {
  default     = []
  type        = list(string)
  description = "When set along with UseBridges, instructs Tor to use the relay at \"IP:ORPort\" as a \"bridge\" relaying into the Tor network. If \"fingerprint\" is provided (using the same format as for DirAuthority), we will verify that the relay running at that location has the right fingerprint. We also use fingerprint to look up the bridge descriptor at the bridge authority, if itcqs provided and if UpdateBridgesFromAuthority is set too."
}

variable "bridge_distribution" {
  default = null
  type    = string
}

variable "bridge_relay" {
  default     = null
  type        = bool
  description = "Sets the relay to act as a \"bridge\" with respect to relaying connections from bridge users to the Tor network. It mainly causes Tor to publish a server descriptor to the bridge database, rather than to the public directory authorities."
}

variable "client_transport_plugin" {
  default     = null
  type        = string
  description = "In its first form, when set along with a corresponding Bridge line, the Tor client forwards its traffic to a SOCKS-speaking proxy on \"IP:PORT\". Itcqs the duty of that proxy to properly forward the traffic to the bridge.  In its second form, when set along with a corresponding Bridge line, the Tor client launches the pluggable transport proxy executable in path-to-binary using options as its command-line options, and forwards its traffic to it. Itcqs the duty of that proxy to properly forward the traffic to the bridge."
}

variable "contact_info" {
  default     = null
  type        = string
  description = "Administrative contact information for this relay or bridge. This line can be used to contact you if your relay or bridge is misconfigured or something else goes wrong. Note that we archive and publish all descriptors containing these lines and that Google indexes them, so spammers might also collect them. You may want to obscure the fact that itcqs an email address and/or generate a new address for this purpose."
}

variable "control_port" {
  default     = null
  type        = string
  description = "If set, Tor will accept connections on this port and allow those connections to control the Tor process using the Tor Control Protocol (described in control-spec.txt in torspec). Note: unless you also specify one or more of HashedControlPassword or CookieAuthentication, setting this option will cause Tor to allow any process on the local host to control it. (Setting both authentication methods means either method is sufficient to authenticate to Tor.) This option is required for many Tor controllers; most use the value of 9051. If a unix domain socket is used, you may quote the path using standard C escape sequences. Set it to \"auto\" to have Tor pick a port for you. (Default: 0)"
}

variable "dir_port" {
  default     = null
  type        = string
  description = "If this option is nonzero, advertise the directory service on this port. Set it to \"auto\" to have Tor pick a port for you. This option can occur more than once, but only one advertised DirPort is supported: all but one DirPort must have the NoAdvertise flag set. (Default: 0)"
}

variable "dir_port_front_page" {
  default     = null
  type        = string
  description = "When this option is set, it takes an HTML file and publishes it as \"/\" on the DirPort. Now relay operators can provide a disclaimer without needing to set up a separate webserver. Therecqs a sample disclaimer in contrib/operator-tools/tor-exit-notice.html."
}

variable "exit_policy" {
  default     = []
  type        = list(string)
  description = "Set an exit policy for this server. Each policy is of the form accept[6]&#124;reject[6] ADDR[/MASK][:PORT]. If /MASK is omitted then this policy just applies to the host given. Instead of giving a host or network you can also use \"*\" to denote the universe (0.0.0.0/0 and ::/128), or *4 to denote all IPv4 addresses, and *6 to denote all IPv6 addresses. PORT can be a single port number, an interval of ports \"FROM_PORT-TO_PORT\", or \"*\". If PORT is omitted, that means \"*\"."
}

variable "exit_relay" {
  default     = null
  type        = bool
  description = "Tells Tor whether to run as an exit relay. If Tor is running as a non-bridge server, and ExitRelay is set to 1, then Tor allows traffic to exit according to the ExitPolicy option (or the default ExitPolicy if none is specified). If ExitRelay is set to 0, no traffic is allowed to exit, and the ExitPolicy option is ignored."
}

variable "ext_or_port" {
  default     = null
  type        = string
  description = "Open this port to listen for Extended ORPort connections from your pluggable transports."
}

variable "nickname" {
  default     = null
  type        = string
  description = "Set the servercqs nickname to 'name'. Nicknames must be between 1 and 19 characters inclusive, and must contain only the characters [a-zA-Z0-9]."
}

variable "or_port" {
  default     = null
  type        = number
  description = "Advertise this port to listen for connections from Tor clients and servers. This option is required to be a Tor server. Set it to \"auto\" to have Tor pick a port for you. Set it to 0 to not run an ORPort at all. This option can occur more than once. (Default: 0)"
}

variable "relay_bandwidth_burst" {
  default     = null
  type        = string
  description = "If not 0, limit the maximum token bucket size (also known as the burst) for _relayed traffic_ to the given number of bytes in each direction. (Default: 0)"
}

variable "relay_bandwidth_rate" {
  default     = null
  type        = string
  description = "If not 0, a separate token bucket limits the average incoming bandwidth usage for _relayed traffic_ on this node to the specified number of bytes per second, and the average outgoing bandwidth usage to that same value. Relayed traffic currently is calculated to include answers to directory requests, but that may change in future versions. (Default: 0)"
}

variable "server_transport_listen_addr" {
  default     = null
  type        = string
  description = "When this option is set, Tor will suggest IP:PORT as the listening address of any pluggable transport proxy that tries to launch transport."
}

variable "server_transport_plugin" {
  default     = null
  type        = string
  description = "The Tor relay launches the pluggable transport proxy in path-to-binary using options as its command-line options, and expects to receive proxied client traffic from it."
}

variable "use_bridges" {
  default     = null
  type        = bool
  description = "When set, Tor will fetch descriptors for each bridge listed in the \"Bridge\" config lines, and use these relays as both entry guards and directory guards. (Default: 0)"
}
