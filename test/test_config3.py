import pytest
import tftest

@pytest.fixture
def plan():
    tf = tftest.TerraformTest(tfdir=".")
    tf.setup()
    return tf.plan(output=True, tf_vars={
        "dir_port": 80,
        "dir_port_front_page": "/html/file",
        "exit_policy": '["accept *:53", "accept *:80", "accept *:443", "reject *:*"]',
        "exit_relay": "true"
    })


def test_config(plan):
    print(plan.outputs["rendered"])

    assert plan.outputs["rendered"] == """DirPort 80
DirPortFrontPage /html/file
ExitPolicy accept *:53
ExitPolicy accept *:80
ExitPolicy accept *:443
ExitPolicy reject *:*
ExitRelay 1
"""
