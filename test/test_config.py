import pytest
import tftest

@pytest.fixture
def plan():
    tf = tftest.TerraformTest(tfdir=".")
    tf.setup()
    return tf.plan(output=True, tf_vars={
        "bridge_relay": "true",
        "contact_info": "human@sr2.uk",
        "ext_or_port": "auto",
        "nickname": "funkybridge",
        "or_port": 9998,
        "server_transport_listen_addr": "obfs4 0.0.0.0:9999",
        "server_transport_plugin": "obfs4 exec /usr/bin/obfs4proxy"
    })


def test_config(plan):
    print(plan.outputs["rendered"])

    assert plan.outputs["rendered"] == """BridgeRelay 1
ContactInfo human@sr2.uk
ExtORPort auto
Nickname funkybridge
ORPort 9998
ServerTransportListenAddr obfs4 0.0.0.0:9999
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy
"""
