import pytest
import tftest

@pytest.fixture
def plan():
    tf = tftest.TerraformTest(tfdir=".")
    tf.setup()
    return tf.plan(output=True, tf_vars={
        "contact_info": "human@sr2.uk",
        "exit_policy": '["reject *:*"]',
        "nickname": "ididntedittheconfig",
        "or_port": 443
    })


def test_config(plan):
    print(plan)

    assert plan.outputs["rendered"] == """ContactInfo human@sr2.uk
ExitPolicy reject *:*
Nickname ididntedittheconfig
ORPort 443
"""
