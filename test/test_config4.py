import pytest
import tftest

@pytest.fixture
def plan():
    tf = tftest.TerraformTest(tfdir=".")
    tf.setup()
    return tf.plan(output=True, tf_vars={
        "bridge": '["obfs4 10.10.15.76:9069 4557D29965677C025F1F075BDA5385D7435DA7DC cert=PmCuf1bLmqapwN4xRgO1JcTPY/Fz2GF5ED264EVTdaodMWAk9UZF7mApHGDxUsg4f29UWQ iat-mode=0", ' +
                  '"obfs4 10.10.21.75:58679 A4DA6BE5B0815876271B85FA9722ED529A8984DB cert=3/Hu2NFLZzP1RWbNKzKUXvXxSPdyxhiojUN0JrKhYdjKZl4rxF5xSIRK96JWEODtgRePZQ iat-mode=0", ' +
                  '"obfs4 10.10.18.24:4548 9B27C21AC30428644624290C16FAD2D3DA99EE3A cert=D5cUgkNsg8KtgHUdX+93s76NX5z6iPLIHsPx60ty1oJi2LMBP+xwwAcQQt3fLjIbSK+wJg iat-mode=0"]',
        "client_transport_plugin": "obfs4 exec /usr/bin/obfs4proxy",
        "use_bridges": "true"
    })


def test_config(plan):
    print(plan.outputs["rendered"])

    assert plan.outputs["rendered"] == """Bridge obfs4 10.10.15.76:9069 4557D29965677C025F1F075BDA5385D7435DA7DC cert=PmCuf1bLmqapwN4xRgO1JcTPY/Fz2GF5ED264EVTdaodMWAk9UZF7mApHGDxUsg4f29UWQ iat-mode=0
Bridge obfs4 10.10.21.75:58679 A4DA6BE5B0815876271B85FA9722ED529A8984DB cert=3/Hu2NFLZzP1RWbNKzKUXvXxSPdyxhiojUN0JrKhYdjKZl4rxF5xSIRK96JWEODtgRePZQ iat-mode=0
Bridge obfs4 10.10.18.24:4548 9B27C21AC30428644624290C16FAD2D3DA99EE3A cert=D5cUgkNsg8KtgHUdX+93s76NX5z6iPLIHsPx60ty1oJi2LMBP+xwwAcQQt3fLjIbSK+wJg iat-mode=0
ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy
UseBridges 1
"""
