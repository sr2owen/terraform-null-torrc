<!-- markdownlint-disable -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.3.0 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bridge"></a> [bridge](#input\_bridge) | When set along with UseBridges, instructs Tor to use the relay at "IP:ORPort" as a "bridge" relaying into the Tor network. If "fingerprint" is provided (using the same format as for DirAuthority), we will verify that the relay running at that location has the right fingerprint. We also use fingerprint to look up the bridge descriptor at the bridge authority, if itcqs provided and if UpdateBridgesFromAuthority is set too. | `list(string)` | `[]` | no |
| <a name="input_bridge_distribution"></a> [bridge\_distribution](#input\_bridge\_distribution) | n/a | `string` | `null` | no |
| <a name="input_bridge_relay"></a> [bridge\_relay](#input\_bridge\_relay) | Sets the relay to act as a "bridge" with respect to relaying connections from bridge users to the Tor network. It mainly causes Tor to publish a server descriptor to the bridge database, rather than to the public directory authorities. | `bool` | `null` | no |
| <a name="input_client_transport_plugin"></a> [client\_transport\_plugin](#input\_client\_transport\_plugin) | In its first form, when set along with a corresponding Bridge line, the Tor client forwards its traffic to a SOCKS-speaking proxy on "IP:PORT". Itcqs the duty of that proxy to properly forward the traffic to the bridge.  In its second form, when set along with a corresponding Bridge line, the Tor client launches the pluggable transport proxy executable in path-to-binary using options as its command-line options, and forwards its traffic to it. Itcqs the duty of that proxy to properly forward the traffic to the bridge. | `string` | `null` | no |
| <a name="input_contact_info"></a> [contact\_info](#input\_contact\_info) | Administrative contact information for this relay or bridge. This line can be used to contact you if your relay or bridge is misconfigured or something else goes wrong. Note that we archive and publish all descriptors containing these lines and that Google indexes them, so spammers might also collect them. You may want to obscure the fact that itcqs an email address and/or generate a new address for this purpose. | `string` | `null` | no |
| <a name="input_control_port"></a> [control\_port](#input\_control\_port) | If set, Tor will accept connections on this port and allow those connections to control the Tor process using the Tor Control Protocol (described in control-spec.txt in torspec). Note: unless you also specify one or more of HashedControlPassword or CookieAuthentication, setting this option will cause Tor to allow any process on the local host to control it. (Setting both authentication methods means either method is sufficient to authenticate to Tor.) This option is required for many Tor controllers; most use the value of 9051. If a unix domain socket is used, you may quote the path using standard C escape sequences. Set it to "auto" to have Tor pick a port for you. (Default: 0) | `string` | `null` | no |
| <a name="input_dir_port"></a> [dir\_port](#input\_dir\_port) | If this option is nonzero, advertise the directory service on this port. Set it to "auto" to have Tor pick a port for you. This option can occur more than once, but only one advertised DirPort is supported: all but one DirPort must have the NoAdvertise flag set. (Default: 0) | `string` | `null` | no |
| <a name="input_dir_port_front_page"></a> [dir\_port\_front\_page](#input\_dir\_port\_front\_page) | When this option is set, it takes an HTML file and publishes it as "/" on the DirPort. Now relay operators can provide a disclaimer without needing to set up a separate webserver. Therecqs a sample disclaimer in contrib/operator-tools/tor-exit-notice.html. | `string` | `null` | no |
| <a name="input_exit_policy"></a> [exit\_policy](#input\_exit\_policy) | Set an exit policy for this server. Each policy is of the form accept[6]&#124;reject[6] ADDR[/MASK][:PORT]. If /MASK is omitted then this policy just applies to the host given. Instead of giving a host or network you can also use "*" to denote the universe (0.0.0.0/0 and ::/128), or *4 to denote all IPv4 addresses, and *6 to denote all IPv6 addresses. PORT can be a single port number, an interval of ports "FROM\_PORT-TO\_PORT", or "*". If PORT is omitted, that means "*". | `list(string)` | `[]` | no |
| <a name="input_exit_relay"></a> [exit\_relay](#input\_exit\_relay) | Tells Tor whether to run as an exit relay. If Tor is running as a non-bridge server, and ExitRelay is set to 1, then Tor allows traffic to exit according to the ExitPolicy option (or the default ExitPolicy if none is specified). If ExitRelay is set to 0, no traffic is allowed to exit, and the ExitPolicy option is ignored. | `bool` | `null` | no |
| <a name="input_ext_or_port"></a> [ext\_or\_port](#input\_ext\_or\_port) | Open this port to listen for Extended ORPort connections from your pluggable transports. | `string` | `null` | no |
| <a name="input_nickname"></a> [nickname](#input\_nickname) | Set the servercqs nickname to 'name'. Nicknames must be between 1 and 19 characters inclusive, and must contain only the characters [a-zA-Z0-9]. | `string` | `null` | no |
| <a name="input_or_port"></a> [or\_port](#input\_or\_port) | Advertise this port to listen for connections from Tor clients and servers. This option is required to be a Tor server. Set it to "auto" to have Tor pick a port for you. Set it to 0 to not run an ORPort at all. This option can occur more than once. (Default: 0) | `number` | `null` | no |
| <a name="input_relay_bandwidth_burst"></a> [relay\_bandwidth\_burst](#input\_relay\_bandwidth\_burst) | If not 0, limit the maximum token bucket size (also known as the burst) for _relayed traffic_ to the given number of bytes in each direction. (Default: 0) | `string` | `null` | no |
| <a name="input_relay_bandwidth_rate"></a> [relay\_bandwidth\_rate](#input\_relay\_bandwidth\_rate) | If not 0, a separate token bucket limits the average incoming bandwidth usage for _relayed traffic_ on this node to the specified number of bytes per second, and the average outgoing bandwidth usage to that same value. Relayed traffic currently is calculated to include answers to directory requests, but that may change in future versions. (Default: 0) | `string` | `null` | no |
| <a name="input_server_transport_listen_addr"></a> [server\_transport\_listen\_addr](#input\_server\_transport\_listen\_addr) | When this option is set, Tor will suggest IP:PORT as the listening address of any pluggable transport proxy that tries to launch transport. | `string` | `null` | no |
| <a name="input_server_transport_plugin"></a> [server\_transport\_plugin](#input\_server\_transport\_plugin) | The Tor relay launches the pluggable transport proxy in path-to-binary using options as its command-line options, and expects to receive proxied client traffic from it. | `string` | `null` | no |
| <a name="input_use_bridges"></a> [use\_bridges](#input\_use\_bridges) | When set, Tor will fetch descriptors for each bridge listed in the "Bridge" config lines, and use these relays as both entry guards and directory guards. (Default: 0) | `bool` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_rendered"></a> [rendered](#output\_rendered) | n/a |
<!-- markdownlint-restore -->
